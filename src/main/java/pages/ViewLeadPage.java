package pages;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import libraries.Annotations;

public class ViewLeadPage extends Annotations {
	@Then("Verify the lead name (.*)")
	public ViewLeadPage verifyFirstName(String fName) {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals(fName)) {
			System.out.println("First name matches with input data "+fName);
		}else {
			System.err.println("First name not matches with input data "+fName);
		}
		return this;
	}
	
	@And("click on the Delete Button")	
	public MyLeadsPage ClickDeleteLead()
	{
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
	
	@And("click on the Edit Button")
	public EditLeadsPage ClickEditLead()
	{
		driver.findElementByLinkText("Edit").click();
		return new EditLeadsPage();
	}
	@And("Click on DuplicateLead Button")
	public DuplicateLeadPage ClickDuplicateButton()
	{
		driver.findElementByLinkText("Duplicate Lead").click();
		return new DuplicateLeadPage();
	}
	
	@And("Verify Duplicate ID")
	public ViewLeadPage verifyDupID()
	{
		String dupleadid = driver.findElementById("viewLead_companyName_sp").getText().replaceAll("\\D", "");
		System.out.println("Lead ID duplicated :" +dupleadid);
		return this;
	}
	
	
	
	
	
}
