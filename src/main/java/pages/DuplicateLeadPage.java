package pages;

import cucumber.api.java.en.And;
import libraries.Annotations;

public class DuplicateLeadPage extends Annotations
{
	
@And("Click on CreateLead to duplicateID")	
public ViewLeadPage clickCreateLead()
{
	driver.findElementByClassName("smallSubmit").click();
	return new ViewLeadPage();
}
	
}
