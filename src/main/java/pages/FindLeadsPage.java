package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import libraries.Annotations;

public class FindLeadsPage extends Annotations
{

	
	@And("Enter the Company name (.*)")
	public FindLeadsPage typeCompName(String cName) throws InterruptedException 
	{
		Thread.sleep(2000);
		driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys(cName);
		Thread.sleep(2000);
		return this;
	}
	@And("Click on the Find Lead Button")
	public FindLeadsPage clickFind() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Find Leads']")));
		//driver.findElementByXPath("//button[text()='Find Leads']").click();
		return this;
	}
		
		/*WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Find Leads']")));
		driver.findElementByXPath("//button[text()='Find Leads']").click();*/
	
	
	@And("Click on the First Lead ID")
	public ViewLeadPage clickLead() throws InterruptedException
	{
		
		driver.findElementByXPath("//table[@class='x-grid3-row-table']").click();
		WebElement table = driver.findElementByXPath("//table[@class='x-grid3-row-table']");
		Thread.sleep(5000);
		List<WebElement> tablerow = table.findElements(By.tagName("tr"));
		int size = tablerow.size();			
		System.out.println(size);
		
		WebElement firstrow = tablerow.get(0);
		List<WebElement> rowdata = firstrow.findElements(By.tagName("td"));
		int size2 = rowdata.size();		
		System.out.println(rowdata.size());
		
		String leadid = rowdata.get(0).getText();
		System.out.println("Lead ID in a first row :" +leadid);
		//Actions act = new Actions(driver);
		//act.click(rowdata.get(1)).perform();
		//Thread.sleep(3000);
		driver.findElementByLinkText(leadid).click();
		
		
		return new ViewLeadPage();
	}
	
	@And("Click on EmailTab")
	public FindLeadsPage clickEmailTab()
	{
		driver.findElementByXPath("//span[text()='Email']").click();
		return this;
	}
	
	@And("Type Email ID")
	public FindLeadsPage typeEmailID() throws InterruptedException 
	{
		
		//driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("test@gmail.com");
		driver.findElementByName("emailAddress").sendKeys("test@gmail.com");
		Thread.sleep(2000);
		return this;
	}
	
	
	}

