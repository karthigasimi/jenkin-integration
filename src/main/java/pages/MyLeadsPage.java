package pages;

import cucumber.api.java.en.And;
import libraries.Annotations;

public class MyLeadsPage extends Annotations {
	@And("Click on the create Lead")
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadPage();
		
	}
	@And("Click on the Find Lead Link")
	public FindLeadsPage ClickFindLead() {
		driver.findElementByLinkText("Find Leads")
		.click();
		return new FindLeadsPage();
	}
	
	@And("Click on the Merge Lead Link")
	public MergeLeadPage ClickMergeLead() {
		driver.findElementByLinkText("Merge Leads")
		.click();
		return new MergeLeadPage();
	}

}
