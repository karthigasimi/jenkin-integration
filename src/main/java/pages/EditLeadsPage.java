package pages;

import cucumber.api.java.en.And;
import libraries.Annotations;

public class EditLeadsPage extends Annotations 
{
	@And("Clear the Company name Field")
	public EditLeadsPage clearCompanyfield()
	{
		driver.findElementById("updateLeadForm_companyName").clear();
		return this;
	}
	@And("Enter the updated Company Name (.*)")
	public EditLeadsPage enterNewCompanyName(String updatedCName)
	{
		driver.findElementById("updateLeadForm_companyName").sendKeys(updatedCName);
		return this;
	}
	@And("Click on update button")
	public ViewLeadPage clickUpdate()
	{
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();
	}
	

}
