package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import libraries.Annotations;

public class MergeLeadPage extends Annotations
{
public MergeFromLeadPage clickFromLead()
{
	driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
	return new MergeFromLeadPage();
}

public MergeToLeadPage clickToLead()
{
	driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
	return new MergeToLeadPage();
}



public void clickMerge()
{
	// TODO Auto-generated method stub
	driver.findElementByLinkText("Merge").click();
	driver.switchTo().alert().accept();
	
//	return new ViewLeadPage();
	
}

/*public ViewLeadPage acceptAlert()
{
	
	
	
	driver.switchTo().alert().accept();
	return new ViewLeadPage();
}*/



}
