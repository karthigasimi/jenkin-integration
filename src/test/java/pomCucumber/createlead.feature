Feature: Create Lead


Scenario Outline: TC001 Positive createlead
When Click on the Lead
And Click on the create Lead
And Enter Comp name <cName>
And Enter Fir Name <fName>
And Enter Las Name <lName>
And click create Lead button 
Then Verify the lead name <fName>
Examples:
|cName|fName|lName|
|Amazon|Karthi|Prabhu|


Scenario Outline: TC002 Positive Deletelead
When Click on the Lead
And Click on the Find Lead Link
And Enter the Company name <cName>
And Click on the Find Lead Button
And Click on the First Lead ID
And click on the Delete Button

Examples:
|cName|
|Amazon|

Scenario Outline: TC003 Positive Editlead
When Click on the Lead
And Click on the Find Lead Link
And Enter the Company name <cName>
And Click on the Find Lead Button
And Click on the First Lead ID
And click on the Edit Button
And Clear the Company name Field
And Enter the updated Company Name <updatedCName>
And Click on update button
Examples:
|cName|updatedCName|
|Amazon|Infosys|

Scenario Outline: TC004 Positive Duplicatelead
When Click on the Lead
And Click on the Find Lead Link
And Click on EmailTab
And Type Email ID
And Click on the Find Lead Button
And Click on the First Lead ID
And Click on DuplicateLead Button
And Click on CreateLead to duplicateID
And Verify Duplicate ID
Examples:
|EmailID|
|tets@gmail.com|

