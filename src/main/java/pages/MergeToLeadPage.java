package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import libraries.Annotations;

public class MergeToLeadPage extends Annotations
{
	Set<String> mergewindow = driver.getWindowHandles();
	List<String> merging = new ArrayList<>(mergewindow);
	
public MergeToLeadPage enterlastName(String lName) throws InterruptedException
{
	//Set<String> mergewindow = driver.getWindowHandles();
	//List<String> merging = new ArrayList<>(mergewindow);
	driver.switchTo().window(merging.get(1));
	
	driver.findElementByXPath("(//input[contains(@class,'x-form-text x-form-field')])[3]").sendKeys(lName);
	Thread.sleep(2000);
	return this;
}


public MergeToLeadPage clickToFindLeadbutton() throws InterruptedException
{
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Find Leads']")));
	return this;	
}


public MergeLeadPage clickToLeadID() throws InterruptedException
{
	
	//driver.findElementByXPath("(//table[@class='x-grid3-row-table'])[1]").click();
	WebElement table = driver.findElementByXPath("(//table[@class='x-grid3-row-table'])[1]");
	Thread.sleep(5000);
	List<WebElement> tablerow = table.findElements(By.tagName("tr"));
	int size = tablerow.size();			
	System.out.println(size);
	
	WebElement firstrow = tablerow.get(0);
	List<WebElement> rowdata = firstrow.findElements(By.tagName("td"));
		
	System.out.println(rowdata.size());
	
	String leadid = rowdata.get(0).getText();
	System.out.println("Lead ID in a first row :" +leadid);
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.elementToBeClickable(By.linkText(leadid)));
	WebElement toleadID = driver.findElementByLinkText(leadid);
	Actions act = new Actions(driver);
	act.moveToElement(toleadID).click().perform();
	driver.switchTo().window(merging.get(0));
	Thread.sleep(2000);

	
	Thread.sleep(2000);
	return new MergeLeadPage();
}
			
	



}
