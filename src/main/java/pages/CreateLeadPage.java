package pages;

import cucumber.api.java.en.And;
import libraries.Annotations;

public class CreateLeadPage extends Annotations {
	@And("Enter Company name (.*)")
	public CreateLeadPage typeCompanyName(String cName) {
		driver.findElementById("createLeadForm_companyName")
		.sendKeys(cName);
		return this;
	}
	@And("Enter First Name (.*)")
	public CreateLeadPage typeFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName")
		.sendKeys(fName);
		return this;
	}
	@And("Enter Last Name (.*)")
	public CreateLeadPage typeLastName(String lName) {
		driver.findElementById("createLeadForm_lastName")
		.sendKeys(lName);
		return this;
	}
		
		@And("Enter Email ID (.*)")
		public CreateLeadPage typeEmailID(String emailId) {
			driver.findElementById("createLeadForm_primaryEmail")
			.sendKeys(emailId);
			return this;
	}
	@And("click create Lead")
	public ViewLeadPage clickCreateLeadButton() {
		driver.findElementByClassName("smallSubmit")
		.click();
		return new ViewLeadPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
