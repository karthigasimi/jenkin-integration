package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC005_DuplicateLead extends Annotations
{

	
	
	@Test()
	public void duplicateLeadTest() throws InterruptedException
	{
		new MyHomePage().clickLeadsTab().ClickFindLead().clickEmailTab().typeEmailID().clickFind().clickLead().ClickDuplicateButton().clickCreateLead().verifyDupID();
		
		//updatedCName is not available in Excel. SO include it in Excel
		
	}
	
}
