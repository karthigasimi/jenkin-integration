package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MergeLeadPage;
import pages.MyHomePage;
import pages.ViewLeadPage;

public class TC003_MergeLead extends Annotations
{
	@BeforeClass
	public void setData() {
		excelFileName = "TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void MergeLeadTest(String cName, String fName, String lName) throws InterruptedException
	{
		new MyHomePage()
		.clickLeadsTab()
		.ClickMergeLead()
		.clickFromLead().enterfirstName(fName).clickFromFindLeadbutton().clickFromLeadID()
		.clickToLead().enterlastName(lName).clickToFindLeadbutton().clickToLeadID().clickMerge();
		
		
	
	}
}
